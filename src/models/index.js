'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
//const Sequelize = require('sequelize-views-support');
const basename = path.basename(__filename);
const config = require(__dirname + '/../config/config.js');
//config = require('../config/config.json')[env];
const db = {};

let sequelize;
sequelize = new Sequelize(config);

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes)
    db[model.name] = model;
  });
Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.ms_employee.hasOne(db.ms_department, { foreignKey: 'id', sourceKey: 'department_id', as: 'department' });
db.ms_user.hasOne(db.ms_employee, { foreignKey: 'id', sourceKey: 'employee_id', as: 'employee' });

db.ms_department.hasMany(db.ms_employee, { foreignKey: 'department_id', sourceKey: 'id', as: 'employeeList' });
db.ms_department.hasMany(db.ms_line, { foreignKey: 'department_id', sourceKey: 'id', as: 'lineList' });

db.ms_line.hasOne(db.ms_department, { foreignKey: 'id', sourceKey: 'department_id', as: 'department' });
db.ms_line.hasMany(db.ms_shift, { foreignKey: 'line_id', sourceKey: 'id', as: 'shiftList' });

db.ms_shift.hasOne(db.ms_line, { foreignKey: 'id', sourceKey: 'line_id', as: 'line' });
db.ms_shift.hasOne(db.ms_employee, { foreignKey: 'id', sourceKey: 'employee_id', as: 'employee' });

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
