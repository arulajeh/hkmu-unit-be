module.exports = (sequelize, DataTypes) => {
  const Ms_shift = sequelize.define('ms_shift', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    employee_id: {
      type: DataTypes.STRING,
      allowNull: false
    },
    line_id: {
      type: DataTypes.STRING,
      allowNull: false
    },
    date: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    start: {
      type: DataTypes.DATE,
    },
    end: {
      type: DataTypes.DATE,
    },
    good: {
      type: DataTypes.INTEGER
    },
    reject: {
      type: DataTypes.INTEGER
    },
    turn: {
      type: DataTypes.INTEGER
    },
    is_approved: {
      type: DataTypes.BOOLEAN,
    },
    is_submitted: {
      type: DataTypes.BOOLEAN,
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    created_by: {
      type: DataTypes.STRING,
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE
    },
    updated_by: {
      type: DataTypes.STRING
    },
  },
    {
      freezeTableName: true,
      timestamps: false
    });
  return Ms_shift;
}
