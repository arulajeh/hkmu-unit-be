module.exports = (sequelize, DataTypes) => {
  const Ms_line = sequelize.define('ms_line', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    department_id: {
      type: DataTypes.STRING,
      allowNull: false
    },
    max_capacity: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    created_by: {
      type: DataTypes.STRING,
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE
    },
    updated_by: {
      type: DataTypes.STRING
    },
  },
  {
    freezeTableName: true,
    timestamps: false
  });
  return Ms_line;
}
