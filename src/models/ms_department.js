module.exports = (sequelize, DataTypes) => {
  const Ms_department = sequelize.define('ms_department', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    created_by: {
      type: DataTypes.STRING,
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE
    },
    updated_by: {
      type: DataTypes.STRING
    },
  },
  {
    freezeTableName: true,
    timestamps: false
  });
  return Ms_department;
}
