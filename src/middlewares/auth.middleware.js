const jwt = require('../utils/jwt')

const authToken = (req, res, next) => {
  const authorization = req.headers.authorization;
  const token = authorization && authorization.split(' ')[1];
  if (!token) return res.status(401).json({ message: 'token_not_found' });

  try {
    const decoded = jwt.verifyJwt(token);

    // Check if token is expired
    const now = new Date().getTime();
    const expired = decoded.exp * 1000;
    if (now > expired) return res.status(401).json({ message: 'token_expired' });

    req.user = decoded;
    next();
  } catch (error) {
    res.status(401).json({ message: 'token_invalid' });
  }
};

module.exports = authToken