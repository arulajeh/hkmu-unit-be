const calculatePagination = (page, limit) => {
  const defaultPage = page || 1;
  const defaultLimit = limit || 10;

  const offset = (defaultPage - 1) * defaultLimit;
  const currentPage = defaultPage;
  const currentLimit = defaultLimit;

  return {
    offset,
    page: currentPage,
    limit: currentLimit,
  };
}

const countDataModel = async (model, conditions) => {
  try {
    return await model.count({ where: conditions });
  } catch (error) {
    throw new Error(error);
  }
};

const rmAttrDb = (data) => {
  delete data.created_by;
  delete data.updated_by;
  delete data.created_at;
  delete data.updated_at;
  return data;
}

module.exports = {
  calculatePagination,
  countDataModel,
  rmAttrDb
}