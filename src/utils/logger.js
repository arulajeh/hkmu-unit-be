const winston = require('winston');

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: 'logs/error.log', level: 'error' }),
    new winston.transports.File({ filename: 'logs/combined.log' })
  ]
})

if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: winston.format.simple()
  }));
}

logger.error = (message) => {
  if (message instanceof Error) {
    logger.log({
      level: 'error',
      message: message.stack,
    })
  } else {
    logger.log({
      level: 'error',
      message,
    })
  }
}

module.exports = logger;