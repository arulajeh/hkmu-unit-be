const jsonwebtoken = require('jsonwebtoken');

const signJwt = (payload) => {
  const secret = process.env.JWT_SECRET;
  const jwtDuration = process.env.JWT_DURATION;
  const options = { expiresIn: jwtDuration };
  return jsonwebtoken.sign(payload, secret, options);
}

const verifyJwt = (token) => {
  const secret = process.env.JWT_SECRET;
  return jsonwebtoken.verify(token, secret);
}

const decodeJwt = (token) => {
  return jsonwebtoken.decode(token);
}

module.exports = {
  signJwt,
  verifyJwt,
  decodeJwt
}