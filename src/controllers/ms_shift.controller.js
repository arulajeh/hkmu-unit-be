const {
  createShift,
  getOneShift,
  getShiftById,
  getAllShift,
  deleteShift,
  updateShift
} = require("../services/ms_shift.service");
const logger = require("../utils/logger");

const getAll = async (req, res) => {
  try {
    const { page, limit } = req.query;
    const { tanggal, departmentid, lineid, employeeid } = req.headers
    const pagination = { page, limit };
    const search = { date: tanggal, department_id: departmentid, line_id: lineid, employee_id: employeeid };
    const withPaging = page && limit;
    const data = await getAllShift(pagination, search, withPaging);
    res.status(200).json({
      status: "success",
      data,
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
}

const checkShift = async (req, res) => {
  try {
    let { line_id, tanggal } = req.headers;
    if (!line_id || !tanggal) return res.status(400).json({ message: "missing_parameter" });
    const isShiftExist = await getOneShift({ line_id, date: tanggal });
    const shiftData = isShiftExist;
    res.json({ message: "get_shift_success", data: shiftData || null });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
}

const create = async (req, res) => {
  try {
    const { employee_id, line_id, date, turn } = req.body;
    const isShiftExist = await getOneShift({ line_id, date, turn });
    if (isShiftExist?.start && !isShiftExist?.end) {
      return res.status(400).json({ message: "shift_already_started", data: isShiftExist });
    }
    // if (isShiftExist) return res.status(400).json({ message: "shift_already_exist" });
    const data = {
      employee_id,
      line_id,
      date,
      turn,
      start: new Date(),
      created_at: new Date(),
      created_by: employee_id,
    }
    const result = await createShift(data);
    res.status(201).json({
      status: "success",
      data: result,
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
}

const update = async (req, res) => {
  try {
    const { id } = req.params;
    const { employee_id, line_id, date, turn, start, end, good, reject, is_approved, is_submitted } = req.body;
    const isShiftExist = await getShiftById(id);
    if (!isShiftExist) return res.status(400).json({ message: "shift_not_exist" });
    const data = {
      employee_id,
      line_id,
      date,
      turn,
      start,
      end,
      good,
      reject,
      is_approved,
      is_submitted,
      updated_at: new Date(),
      updated_by: req.user.id,
    }
    const result = await updateShift(id, data);
    res.status(200).json({
      status: "success",
      data: result,
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
}

const startShift = async (req, res) => {
  try {
    const { employee_id, date, line_id, turn } = req.body;
    const isShiftExist = await getOneShift({ line_id, date });
    if (isShiftExist?.start && !isShiftExist?.end) {
      return res.status(400).json({ message: "shift_already_started", data: isShiftExist });
    }
    const data = {
      employee_id,
      line_id,
      date,
      turn,
      start: new Date(),
      created_at: new Date(),
      created_by: employee_id,
    }
    const result = await createShift(data);
    res.status(201).json({
      status: "start_shift_success",
      data: result,
    })
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
}

const endShift = async (req, res) => {
  try {
    const { shift_id, good, employee_id } = req.body;
    const isShiftExist = await getShiftById(shift_id);
    if (!isShiftExist) return res.status(400).json({ message: "shift_not_exist" });
    if (isShiftExist.end) return res.status(400).json({ message: "shift_already_ended", data: isShiftExist });
    if (!isShiftExist.start) return res.status(400).json({ message: "shift_not_started", data: isShiftExist });
    const data = {
      end: new Date(),
      good,
      updated_at: new Date(),
      updated_by: employee_id,
    }
    const result = await updateShift(shift_id, data);
    res.status(200).json({
      status: "end_shift_success",
      data: result,
    })
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
}

// const getShift = async (req, res) => { }

// const updateShift = async (req, res) => { }

// const deleteShift = async (req, res) => { }

module.exports = {
  getAll,
  checkShift,
  startShift,
  endShift,
  create,
  update
  // getShift,
  // createShift,
  // updateShift,
  // deleteShift,
};