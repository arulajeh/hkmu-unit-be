const msUserSvc = require('../services/ms_user.service');
const { verifyPassword } = require('../utils/password');
const { signJwt } = require('../utils/jwt');
const logger = require('../utils/logger');

const login = async (req, res) => {
  try {
    const { username, password } = req.body;
    const isUserExist = await msUserSvc.isUserExist(username);
    if (!isUserExist) return res.status(400).json({ message: 'user_not_found' });
  
    const isPasswordMatch = verifyPassword(password, isUserExist.password);
    if (!isPasswordMatch) return res.status(400).json({ message: 'password_not_match' });
  
    delete isUserExist.password;
    const user = { id: isUserExist.id, username: isUserExist.username, employee: isUserExist.employee };
    const token = signJwt(user);
    res.json({ message: 'Login', token, user });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: 'internal_server_error' });
  }
}

const logout = async (req, res) => {
  res.json({ message: 'Logout' });
}

const register = async (req, res) => {
  try {
    const { username, password, employee_id } = req.body;
    const isUserExist = await msUserSvc.isUserExist(username);
    if (isUserExist) {
      return res.status(400).json({ message: 'user_already_registered' });
    }
    const user = {
      username,
      password,
      employee_id,
      created_by: req.user.id
    }
    const newUser = await msUserSvc.register(user);
    delete newUser.password;
    res.json({
      message: 'register_successfully',
      user: {
        id: newUser.id,
        username: newUser.username,
        employee_id: newUser.employee_id
      }
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: 'internal_server_error' });
  }
}

module.exports = {
  login,
  logout,
  register
}