const {
  updateUser,
  deleteUser,
  isUserExist
} = require('../services/ms_user.service');
const {
  updateEmployee,
  deleteEmployee
} = require('../services/ms_employee.service');
const {
  verifyPassword,
  hashPassword
} = require('../utils/password');
const logger = require('../utils/logger');

const updatePassword = async (req, res) => {
  try {
    const { id } = req.params;
    const { existing_password, password } = req.body;
    const isExist = await isUserExist(id);
    if (!isExist) return res.status(400).json({ message: 'user_not_found' });
    const isPasswordMatch = verifyPassword(existing_password, isExist.password);
    if (!isPasswordMatch) return res.status(400).json({ message: 'wrong_password' });
    const data = { password: hashPassword(password) };
    const user = await updateUser(id, data);
    res.status(200).json({ message: 'password_updated', data: user });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
}

const updateProfile = async (req, res) => {
  try {
    const { id } = req.params;
    const { name, title } = req.body;
    const data = { name, title };
    const user = await updateEmployee(id, data);
    res.status(200).json({ message: 'profile_updated', data: user });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
}

const deleteUserEmployee = async (req, res) => {
  try {
    const { id } = req.params;
    const user = await isUserExist(id);
    await deleteUser(id);
    await deleteEmployee(user.employee.id);
    res.status(200).json({ message: 'user_deleted' });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
}

module.exports = {
  updatePassword,
  updateProfile,
  deleteUserEmployee
}