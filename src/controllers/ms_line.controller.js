const {
  createLine,
  deleteLine,
  getAllLine,
  getByDeptId,
  getLineById,
  updateLine,
} = require("../services/ms_line.service");
const logger = require("../utils/logger");

const getAll = async (req, res) => {
  try {
    const { page, limit, name } = req.query;
    const { departmentid } = req.headers;
    const pagination = { page, limit };
    const search = { name, department_id: departmentid };
    const withPaging = page && limit;
    const result = await getAllLine(pagination, search, withPaging);
    res.status(200).json({
      message: "get_line_success",
      data: result.data,
      pagination: result.pagination,
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
};

const getById = async (req, res) => {
  try {
    const { id } = req.params;
    const result = await getLineById(id);
    if (!result) return res.status(400).json({ message: "line_not_found" });
    res.status(200).json({
      message: "get_line_success",
      data: result,
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
};

const create = async (req, res) => {
  try {
    const { name, department_id, max_capacity } = req.body;
    const data = {
      name,
      department_id,
      max_capacity,
      created_at: new Date(),
      created_by: req.user.id,
    };
    const result = await createLine(data);
    res.status(200).json({
      message: "create_line_success",
      data: result,
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
};

const update = async (req, res) => {
  try {
    const { id } = req.params;
    const { name, department_id } = req.body;
    const isLineExist = await getLineById(id);
    if (!isLineExist) {
      return res.status(400).json({ message: "line_not_found" });
    }
    const data = {
      name,
      department_id,
      updated_at: new Date(),
      updated_by: req.user.id,
    };
    const result = await updateLine(id, data);
    res.status(200).json({
      message: "update_line_success",
      data: result,
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
};

const remove = async (req, res) => {
  try {
    const { id } = req.params;
    const isLineExist = await getLineById(id);
    if (!isLineExist) {
      return res.status(400).json({ message: "line_not_found" });
    }
    const result = await deleteLine(id);
    res.status(200).json({
      message: "delete_line_success",
      data: result,
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
};

const getByDepartmentId = async (req, res) => {
  try {
    const { department_id } = req.params;
    const result = await getByDeptId(department_id);
    res.status(200).json({
      message: "get_line_success",
      data: result,
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove,
  getByDepartmentId,
};
