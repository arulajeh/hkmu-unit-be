const {
  getAllDepartment,
  getDepartmentById,
  createDepartment,
  updateDepartment,
  deleteDepartment,
} = require("../services/ms_department.service");
const { getByDeptId } = require("../services/ms_line.service");
const logger = require("../utils/logger");

const getAll = async (req, res) => {
  try {
    const { page, limit, name } = req.query;
    const pagination = { page, limit };
    const search = { name };
    const withPaging = page && limit;
    const result = await getAllDepartment(pagination, search, withPaging);
    res.status(200).json({
      message: "get_department_success",
      data: result.data,
      pagination: result.pagination,
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
};

const getById = async (req, res) => {
  try {
    const { id } = req.params;
    const result = await getDepartmentById(id);
    if (!result) return res.status(400).json({ message: "department_not_found" });
    res.status(200).json({
      message: "get_department_success",
      data: result,
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
};

const create = async (req, res) => {
  try {
    const { name } = req.body;
    const data = {
      name,
      created_at: new Date(),
      created_by: req.user.id,
    };
    const result = await createDepartment(data);
    res.status(200).json({
      message: "create_department_success",
      data: result,
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
};

const update = async (req, res) => {
  try {
    const { id } = req.params;
    const { name } = req.body;
    const isDepartmentExist = await getDepartmentById(id);
    if (!isDepartmentExist) {
      return res.status(400).json({ message: "department_not_found" });
    }
    const data = {
      name,
      updated_at: new Date(),
      updated_by: req.user.id,
    };
    const result = await updateDepartment(id, data);
    res.status(200).json({
      message: "update_department_success",
      data: {
        id: result.id,
        name: result.name,
      },
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
};

const remove = async (req, res) => {
  try {
    const { id } = req.params;

    const isDepartmentExist = await getDepartmentById(id);
    if (!isDepartmentExist) return res.status(400).json({ message: "department_not_found" });

    const isUsed = await getByDeptId(id);
    if (isUsed.length > 0) return res.status(400).json({ message: "department_used" });

    const result = await deleteDepartment(id);
    res.status(200).json({
      message: "delete_department_success",
      data: result,
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: "internal_server_error" });
  }
};

module.exports = {
  getAll,
  create,
  update,
  getById,
  remove,
};
