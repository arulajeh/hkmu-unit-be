const {
  createEmployee,
  deleteEmployee,
  getAllEmployee,
  getEmployeeById,
  updateEmployee,
} = require('../services/ms_employee.service');
const logger = require("../utils/logger");

const getAll = async (req, res) => {
  try {
    const { page, limit, name } = req.query;
    const pagination = { page, limit };
    const { departmentid, title } = req.headers;
    const search = { name, department_id: departmentid, title };
    const withPaging = page && limit;
    const result = await getAllEmployee(pagination, search, withPaging);
    res.status(200).json({
      message: 'get_employee_success',
      data: result.data,
      pagination: result.pagination,
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: 'internal_server_error' });
  }
}

const getById = async (req, res) => {
  try {
    const { id } = req.params;
    const result = await getEmployeeById(id);
    if (!result) return res.status(400).json({ message: 'employee_not_found' });
    res.status(200).json({
      message: 'get_employee_success',
      data: result,
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: 'internal_server_error' });
  }
}

const create = async (req, res) => {
  try {
    const { name, title, department_id } = req.body;
    const data = {
      name,
      title,
      department_id,
      created_at: new Date(),
      created_by: req.user.id,
    };
    const result = await createEmployee(data);
    res.status(200).json({
      message: 'create_employee_success',
      data: result,
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: 'internal_server_error' });
  }
}

const update = async (req, res) => {
  try {
    const { id } = req.params;
    const { name, title, department_id } = req.body;
    const isEmployeeExist = await getEmployeeById(id);
    if (!isEmployeeExist) return res.status(400).json({ message: 'employee_not_found' });
    const data = {
      name,
      title,
      department_id,
      updated_at: new Date(),
      updated_by: req.user.id,
    };
    const result = await updateEmployee(id, data);
    res.status(200).json({
      message: 'update_employee_success',
      data: result,
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: 'internal_server_error' });
  }
}

const remove = async (req, res) => {
  try {
    const { id } = req.params;
    const isEmployeeExist = await getEmployeeById(id);
    if (!isEmployeeExist) return res.status(400).json({ message: 'employee_not_found' });
    const result = await deleteEmployee(id);
    res.status(200).json({
      message: 'delete_employee_success',
      data: result,
    });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ message: 'internal_server_error' });
  }
}

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove,
};