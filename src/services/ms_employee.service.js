const { ms_employee, ms_department, ms_line } = require("../models");
const { Op } = require("sequelize");
const { calculatePagination, countDataModel } = require("../utils/common");
const uuid = require("uuid");
const { rmAttrDb } = require("../utils/common");
const logger = require("../utils/logger");

const getAllEmployee = async (pagination, search, withPaging) => {
  try {
    const paginationData = calculatePagination(
      pagination.page,
      pagination.limit
    );
    const name = search?.name;
    const department_id = search?.department_id;
    const title = search?.title;
    const where = [];
    if (name) where.push({ name: { [Op.iLike]: `%${name}%` } });
    if (department_id) where.push({ department_id });
    if (title) where.push({ title: { [Op.iLike]: `%${title}%` } });
    const conditions = { [Op.and]: where };

    const data = await ms_employee.findAll({
      where: conditions,
      order: [["name", "ASC"]],
      attributes: ["id", "name", "title"],
      limit: withPaging ? paginationData.limit : null,
      offset: withPaging ? paginationData.offset : null,
      include: [
        { model: ms_department, as: "department", attributes: ["id", "name"] },
      ]
    });
    const count = await countDataModel(ms_employee, conditions);
    const paginationResult = {
      page: parseInt(paginationData.page),
      limit: parseInt(paginationData.limit),
      total: count,
      totalPage: Math.ceil(count / paginationData.limit),
    };
    return { data, pagination: paginationResult };
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
}

const getEmployeeById = async (id) => {
  try {
    return await ms_employee.findOne({
      where: { id },
      attributes: ["id", "name", "title"],
      include: [
        { model: ms_department, as: "department", attributes: ["id", "name"] },
      ]
    });
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
}

const createEmployee = async (data) => {
  try {
    const { name, title, department_id, created_by } = data;
    const id = "emp-" + uuid.v4();
    const newData = {
      id,
      name,
      title,
      department_id,
      created_by,
      created_at: new Date(),
    };
    const created = await ms_employee.create(newData, { returning: true });
    return rmAttrDb(created.get({ plain: true }));
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
}

const updateEmployee = async (id, data) => {
  try {
    const { name, title, department_id, updated_by } = data;
    const newData = {
      name,
      title,
      department_id,
      updated_by,
      updated_at: new Date(),
    };
    const updated = await ms_employee.update(newData, {
      where: { id },
      returning: true,
    });
    return rmAttrDb(updated[1][0].get({ plain: true }));
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
}

const deleteEmployee = async (id) => {
  try {
    return await ms_employee.destroy({ where: { id } });
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
}

module.exports = {
  getAllEmployee,
  getEmployeeById,
  createEmployee,
  updateEmployee,
  deleteEmployee,
}