const { ms_line, ms_department } = require("../models");
const { Op } = require("sequelize");
const { calculatePagination, countDataModel } = require("../utils/common");
const uuid = require("uuid");
const { rmAttrDb } = require("../utils/common");
const logger = require("../utils/logger");

const getAllLine = async (pagination, search, withPaging) => {
  try {
    const paginationData = calculatePagination(
      pagination.page,
      pagination.limit
    );
    const name = search?.name;
    const department_id = search?.department_id;
    const where = [];
    if (name) where.push({ name: { [Op.iLike]: `%${name}%` } });
    if (department_id) where.push({ department_id });
    const conditions = {
      [Op.and]: where
    };
    const data = await ms_line.findAll({
      where: conditions,
      order: [["name", "ASC"]],
      attributes: ["id", "name", "max_capacity"],
      limit: withPaging ? paginationData.limit : null,
      offset: withPaging ? paginationData.offset : null,
      include: [
        { model: ms_department, as: "department", attributes: ["id", "name"] },
      ]
    });
    const count = await countDataModel(ms_line, conditions);
    const paginationResult = {
      page: parseInt(paginationData.page),
      limit: parseInt(paginationData.limit),
      total: count,
      totalPage: Math.ceil(count / paginationData.limit),
    };
    return { data, pagination: paginationResult };
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
};

const getByDeptId = async (department_id) => {
  try {
    return await ms_line.findAll({
      where: { department_id },
      attributes: ["id", "name", "max_capacity"],
      include: [
        { model: ms_department, as: "department", attributes: ["id", "name"] },
      ]
    });
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
};

const getLineById = async (id) => {
  try {
    return await ms_line.findOne({
      where: { id },
      attributes: ["id", "name", "max_capacity"],
      include: [
        { model: ms_department, as: "department", attributes: ["id", "name"] },
      ]
    });
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
};

const createLine = async (data) => {
  try {
    const line = {
      id: "lin-" + uuid.v4(),
      name: data.name,
      department_id: data.department_id,
      max_capacity: data.max_capacity,
      created_at: new Date(),
      created_by: data.created_by,
    };
    const created = await ms_line.create(line);
    return rmAttrDb(created.get({ plain: true }));
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
};

const updateLine = async (id, data) => {
  try {
    const line = {
      name: data.name,
      department_id: data.department_id,
      max_capacity: data.max_capacity,
      updated_at: new Date(),
      updated_by: data.updated_by,
    };
    const updated = await ms_line.update(line, {
      where: { id },
      returning: true,
    });
    return rmAttrDb(updated[1][0].get({ plain: true }));
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
};

const deleteLine = async (id) => {
  try {
    return await ms_line.destroy({ where: { id } });
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
};

module.exports = {
  getByDeptId,
  getAllLine,
  getLineById,
  createLine,
  updateLine,
  deleteLine,
};
