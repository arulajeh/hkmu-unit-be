const { ms_department, ms_employee } = require("../models");
const { Op } = require("sequelize");
const {
  calculatePagination,
  countDataModel,
  rmAttrDb,
} = require("../utils/common");
const uuid = require("uuid");
const logger = require("../utils/logger");

const getAllDepartment = async (pagination, search, withPaging) => {
  try {
    const paginationData = calculatePagination(
      pagination.page,
      pagination.limit
    );
    const name = search?.name || "";
    const conditions = {
      [Op.and]: [
        { name: { [Op.iLike]: `%${name}%` } },
      ],
    };
    const data = await ms_department.findAll({
      where: conditions,
      order: [
        ["name", "ASC"],
        [{ model: ms_employee, as: "employeeList" }, "name", "ASC"],
      ],
      attributes: ["id", "name"],
      limit: withPaging ? paginationData.limit : null,
      offset: withPaging ? paginationData.offset : null,
      include: [
        { model: ms_employee, as: "employeeList", attributes: ["id", "name", "title"] },
      ],
    });
    const count = await countDataModel(ms_department, conditions);
    const paginationResult = {
      page: parseInt(paginationData.page),
      limit: parseInt(paginationData.limit),
      total: count,
      totalPage: Math.ceil(count / paginationData.limit),
    };
    return { data, pagination: paginationResult };
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
};

const getDepartmentById = async (id) => {
  try {
    return await ms_department.findOne({
      where: { id },
      attributes: ["id", "name"],
      include: [
        { model: ms_employee, as: "employeeList", attributes: ["id", "name", "title"] },
      ],
    });
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
};

const createDepartment = async (data) => {
  try {
    const dept = {
      id: "dpt-" + uuid.v4(),
      name: data.name,
      created_at: new Date(),
      created_by: data.created_by,
    };
    const created = await ms_department.create(dept, { returning: true });
    return rmAttrDb(created.get({ plain: true }));
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
};

const updateDepartment = async (id, data) => {
  try {
    const update = await ms_department.update(data, {
      where: { id },
      returning: true,
    });
    return rmAttrDb(update[1][0].get({ plain: true }));
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
};

const deleteDepartment = async (id) => {
  try {
    return await ms_department.destroy({ where: { id } });
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
};

module.exports = {
  getAllDepartment,
  getDepartmentById,
  createDepartment,
  updateDepartment,
  deleteDepartment,
};
