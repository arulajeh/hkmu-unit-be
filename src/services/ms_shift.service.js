const { ms_shift, ms_line, ms_employee, ms_department } = require("../models");
const { Op } = require("sequelize");
const { calculatePagination, countDataModel } = require("../utils/common");
const uuid = require("uuid");
const { rmAttrDb } = require("../utils/common");
const logger = require("../utils/logger");

const getAllShift = async (pagination, search, withPaging) => {
  try {
    const paginationData = calculatePagination(
      pagination.page,
      pagination.limit
    );
    const line_id = search?.line_id || "";
    const employee_id = search?.employee_id;
    const date = search?.date;
    const is_approved = search?.is_approved;
    const is_submitted = search?.is_submitted;
    const where = [];

    if (line_id) { where.push({ line_id }); }
    if (employee_id) { where.push({ employee_id }); }
    if (date) { where.push({ date }); }
    if (is_approved !== undefined) { where.push({ is_approved }); }
    if (is_submitted !== undefined) { where.push({ is_submitted }); }

    const conditions = { [Op.and]: where };
    const data = await ms_shift.findAll({
      where: conditions,
      order: [["date", "DESC"]],
      attributes: ["id", "employee_id", "line_id", "date", "start", "end", "good", "reject", "turn", "is_approved", "is_submitted"],
      limit: withPaging ? paginationData.limit : null,
      offset: withPaging ? paginationData.offset : null,
      include: [
        {
          model: ms_line,
          as: "line",
          attributes: ["id", "name"],
          include: {
            model: ms_department,
            as: 'department',
            attributes: ["id", "name"]
          }
        },
        { model: ms_employee, as: "employee", attributes: ["id", "name", "title"] },
      ]
    });
    const count = await countDataModel(ms_shift, conditions);
    const paginationResult = {
      page: parseInt(paginationData.page),
      limit: parseInt(paginationData.limit),
      total: count,
      totalPage: Math.ceil(count / paginationData.limit),
    };
    return { data, pagination: paginationResult };
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
}

const getOneShift = async (search) => {
  try {
    const line_id = search?.line_id;
    const employee_id = search?.employee_id;
    const date = search?.date;
    const is_approved = search?.is_approved;
    const is_submitted = search?.is_submitted;
    const turn = search?.turn;
    const where = [];

    if (line_id) { where.push({ line_id }); }
    if (employee_id) { where.push({ employee_id }); }
    if (date) { where.push({ date }); }
    if (is_approved !== undefined) { where.push({ is_approved }); }
    if (is_submitted !== undefined) { where.push({ is_submitted }); }
    if (turn) { where.push({ turn }); }

    const conditions = { [Op.and]: where };
    const shift = await ms_shift.findOne({
      where: conditions,
      attributes: ["id", "employee_id", "line_id", "date", "start", "end", "good", "reject", "turn", "is_approved", "is_submitted"],
      order: [["created_at", "DESC"]],
      include: [
        {
          model: ms_line,
          as: "line",
          attributes: ["id", "name"],
          include: {
            model: ms_department,
            as: 'department',
            attributes: ["id", "name"]
          }
        },
        { model: ms_employee, as: "employee", attributes: ["id", "name", "title"] },
      ]
    });
    return shift?.get({ plain: true });
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
}

const getShiftById = async (id) => {
  try {
    const shift = await ms_shift.findOne({
      where: { id },
      attributes: ["id", "employee_id", "line_id", "date", "start", "end", "good", "reject", "turn", "is_approved", "is_submitted"],
    });
    return shift?.get({ plain: true });
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
}

const createShift = async (data) => {
  try {
    const id = "shf-" + uuid.v4();
    const payload = { id, ...data };
    const created = await ms_shift.create(payload);
    return rmAttrDb(created.get({ plain: true }));
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
}

const updateShift = async (id, data) => {
  try {
    const payload = rmAttrDb(data);
    const updated = await ms_shift.update(payload, { where: { id }, returning: true });
    return rmAttrDb(updated[1][0].get({ plain: true }));
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
}

const deleteShift = async (id) => {
  try {
    return await ms_shift.destroy({ where: { id } });
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
}

module.exports = {
  getAllShift,
  getOneShift,
  getShiftById,
  createShift,
  updateShift,
  deleteShift,
}