const { ms_user, ms_employee } = require('../models');
const uuid = require('uuid');
const { hashPassword } = require('../utils/password');
const logger = require('../utils/logger');
const { calculatePagination, countDataModel } = require("../utils/common");

const getAllUser = async (pagination, search, withPaging) => {
  try {
    const name = search?.name || '';
    const conditions = {
      [Op.and]: [
        { name: { [Op.iLike]: `%${name}%` } },
      ],
    };
    const paginationData = calculatePagination(
      pagination.page,
      pagination.limit
    );
    const data = await ms_user.findAll({
      where: conditions,
      order: [
        ['name', 'ASC'],
        [{ model: ms_employee, as: 'employeeList' }, 'name', 'ASC'],
      ],
      attributes: ['id', 'name'],
      limit: withPaging ? paginationData.limit : null,
      offset: withPaging ? paginationData.offset : null,
      include: [
        { model: ms_employee, as: 'employeeList', attributes: ['id', 'name', 'title'] },
      ],
    });
    const count = await countDataModel(ms_user, conditions);
    const paginationResult = {
      page: parseInt(paginationData.page),
      limit: parseInt(paginationData.limit),
      total: count,
      totalPage: Math.ceil(count / paginationData.limit),
    };
    return { data, pagination: paginationResult };
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
}

const isUserExist = async (username) => {
  try {
    const user = await ms_user.findOne({
      where: { username },
      attributes: ['id', 'username', 'password'],
      include: [{ model: ms_employee, as: 'employee', attributes: ['id', 'name', 'title'] }]
    });
    return user ? user.get({ plain: true }) : false;
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
}

const register = async (data) => {
  try {
    const user = {
      id: 'usr-' + uuid.v4(),
      username: data.username,
      password: hashPassword(data.password),
      employee_id: data.employee_id,
      created_by: data.created_by
    }
    const newUser = await ms_user.create(user, { returning: true });
    return newUser.get({ plain: true });
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
}

const updateUser = async (id, data) => {
  try {
    const user = await ms_user.update(data, { where: { id }, returning: true });
    return user[1][0].get({ plain: true });
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
}

const deleteUser = async (id) => {
  try {
    const user = await ms_user.destroy({ where: { id } });
    return user;
  } catch (error) {
    logger.error(error);
    throw new Error(error);
  }
}

module.exports = {
  getAllUser,
  isUserExist,
  register,
  updateUser,
  deleteUser
}