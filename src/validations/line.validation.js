const yup = require('yup');
const uuid = require('uuid');

yup.addMethod(yup.string, 'isUuid', function(text) {
  return this.test('isUuid', text, function(value) {
    if(!value) return true;
    value = value.substring(4);
    return uuid.validate(value);
  });
})

const lineCreateSchema = yup.object().shape({
  name: yup.string().required("name_required").trim().min(3, "name_min_3").max(50, "name_max_50"),
  department_id: yup.string().required("department_id_required").trim().isUuid("department_id_invalid"),
  max_capacity: yup.number().required("max_capacity_required").min(1, "max_capacity_min_1")
})

const lineUpdateSchema = yup.object().shape({
  name: yup.string().trim().min(3, "name_min_3").max(50, "name_max_50"),
  department_id: yup.string().trim().isUuid("department_id_invalid"),
  max_capacity: yup.number().min(1, "max_capacity_min_1")
})

module.exports = {
  lineCreateSchema,
  lineUpdateSchema
}