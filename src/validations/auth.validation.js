const yup = require('yup');
const uuid = require('uuid');

yup.addMethod(yup.string, 'isUuid', function(text) {
  return this.test('isUuid', text, function(value) {
    if(!value) return true;
    value = value.substring(4);
    return uuid.validate(value);
  });
})

const loginSchema = yup.object().shape({
  username: yup.string().required("username_required").trim().min(6, "username_min_6").max(16, "username_max_16"),
  password: yup.string().required("password_required").trim().min(6, "password_min_6").max(16, "password_max_16")
});

const registerSchema = yup.object().shape({
  username: yup.string().required("username_required").trim().min(6, "username_min_6").max(16, "username_max_16"),
  password: yup.string().required("password_required").trim().min(6, "password_min_6").max(16, "password_max_16"),
  employee_id: yup.string().required().trim().isUuid("employee_id_invalid")
});

module.exports = {
  loginSchema,
  registerSchema
}