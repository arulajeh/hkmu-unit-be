const yup = require('yup');
const uuid = require('uuid');

yup.addMethod(yup.string, 'isUuid', function(text) {
  return this.test('isUuid', text, function(value) {
    if(!value) return true;
    value = value.substring(4);
    return uuid.validate(value);
  });
})

const updatePasswordSchema = yup.object().shape({
  old_password: yup.string().required("old_password_required").trim().min(8, "old_password_min_8").max(50, "old_password_max_50"),
  new_password: yup.string().required("new_password_required").trim().min(8, "new_password_min_8").max(50, "new_password_max_50"),
  confirm_password: yup.string().required("confirm_password_required").trim().min(8, "confirm_password_min_8").max(50, "confirm_password_max_50")
})

module.exports = {
  updatePasswordSchema,

}