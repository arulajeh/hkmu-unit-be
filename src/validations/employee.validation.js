const yup = require('yup');
const uuid = require('uuid');

yup.addMethod(yup.string, 'isUuid', function(text) {
  return this.test('isUuid', text, function(value) {
    if(!value) return true;
    value = value.substring(4);
    return uuid.validate(value);
  });
})

const employeeCreateSchema = yup.object().shape({
  name: yup.string().required("name_required").trim().min(3, "name_min_3").max(50, "name_max_50"),
  title: yup.string().required("title_required").trim().min(3, "title_min_3").max(50, "title_max_50"),
  department_id: yup.string().required("department_id_required").trim().isUuid("department_id_invalid"),
})

const employeeUpdateSchema = yup.object().shape({
  name: yup.string().trim().min(3, "name_min_3").max(50, "name_max_50"),
  title: yup.string().trim().min(3, "title_min_3").max(50, "title_max_50"),
  department_id: yup.string().trim().isUuid("department_id_invalid"),
})

module.exports = {
  employeeCreateSchema,
  employeeUpdateSchema
}