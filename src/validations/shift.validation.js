const yup = require('yup');
const uuid = require('uuid');

yup.addMethod(yup.string, 'isUuid', function(text) {
  return this.test('isUuid', text, function(value) {
    if(!value) return true;
    value = value.substring(4);
    return uuid.validate(value);
  });
})

const shiftCreateSchema = yup.object().shape({
  employee_id: yup.string().required("employee_id_required").trim().isUuid("employee_id_invalid"),
  line_id: yup.string().required("line_id_required").trim().isUuid("line_id_invalid"),
  date: yup.date().required("date_required").typeError("date_invalid"),
  turn: yup.number().required("turn_required").min(1, "turn_min_1").max(3, "turn_max_3")
})

const shiftUpdateSchema = yup.object().shape({
  employee_id: yup.string().trim().isUuid("employee_id_invalid"),
  line_id: yup.string().trim().isUuid("line_id_invalid"),
  date: yup.date().typeError("date_invalid"),
  turn: yup.number().min(1, "turn_min_1").max(3, "turn_max_3"),
  date: yup.date().typeError("date_invalid"),
  start: yup.date().typeError("start_invalid"),
  end: yup.date().typeError("end_invalid"),
  good: yup.number().min(0, "good_min_0"),
  reject: yup.number().min(0, "reject_min_0"),
  is_approved: yup.boolean(),
  is_active: yup.boolean()
})

const shiftStartSchema = yup.object().shape({
  employee_id: yup.string().required("employee_id_required").trim().isUuid("employee_id_invalid"),
  line_id: yup.string().required("line_id_required").trim().isUuid("line_id_invalid"),
  date: yup.date().required("date_required").typeError("date_invalid"),
  turn: yup.number().required("turn_required").min(1, "turn_min_1").max(3, "turn_max_3")
})

const shiftEndSchema = yup.object().shape({
  shift_id: yup.string().required("shift_id_required").trim().isUuid("shift_id_invalid"),
  good: yup.number().required("good_required").min(0, "good_min_0"),
  employee_id: yup.string().required("employee_id_required").trim().isUuid("employee_id_invalid")
})

module.exports = {
  shiftCreateSchema,
  shiftUpdateSchema,
  shiftStartSchema,
  shiftEndSchema
}