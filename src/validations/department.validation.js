const yup = require('yup');
const uuid = require('uuid');

yup.addMethod(yup.string, 'isUuid', function(text) {
  return this.test('isUuid', text, function(value) {
    if(!value) return true;
    value = value.substring(4);
    return uuid.validate(value);
  });
})

const departmentCreateSchema = yup.object().shape({
  name: yup.string().required("name_required").trim()
})

const departmentUpdateSchema = yup.object().shape({
  name: yup.string().required("name_required").trim()
})

module.exports = {
  departmentCreateSchema,
  departmentUpdateSchema
}