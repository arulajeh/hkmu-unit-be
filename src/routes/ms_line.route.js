const express = require("express");
const router = express.Router();
const {
  getAll,
  create,
  getById,
  remove,
  update,
  getByDepartmentId,
} = require("../controllers/ms_line.controller");

const {
  lineCreateSchema,
  lineUpdateSchema,
} = require("../validations/line.validation");
const validation = require("../middlewares/validation.middleware");
const authToken = require("../middlewares/auth.middleware");

router
  .get("/", getAll)
  .post("/", authToken, validation(lineCreateSchema), create)
  .get("/:id", getById)
  .delete("/:id", authToken, remove)
  .put("/:id", authToken, validation(lineUpdateSchema), update)
  .get("/department/:department_id", getByDepartmentId);

module.exports = router;
