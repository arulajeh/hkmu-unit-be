const express = require("express");
const router = express.Router();
const {
  create,
  getAll,
  getById,
  remove,
  update,
} = require("../controllers/ms_employee.controller");

const { employeeCreateSchema, employeeUpdateSchema } = require("../validations/employee.validation");
const validation = require("../middlewares/validation.middleware");
const authToken = require("../middlewares/auth.middleware");

router
  .get("/", getAll)
  .post("/", authToken, validation(employeeCreateSchema), create)
  .get("/:id", getById)
  .delete("/:id", authToken, remove)
  .put("/:id", authToken, validation(employeeUpdateSchema), update);

module.exports = router;