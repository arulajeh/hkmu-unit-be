const express = require("express");
const router = express.Router();
const {
  deleteUserEmployee,
  updatePassword,
  updateProfile
} = require("../controllers/ms_user.controller");

const { updatePasswordSchema } = require("../validations/user.validation");
const { employeeUpdateSchema } = require("../validations/employee.validation");
const validation = require("../middlewares/validation.middleware");
const authToken = require("../middlewares/auth.middleware");

router
  .put("/password/:id", authToken, validation(updatePasswordSchema), updatePassword)
  .put("/profile/:id", authToken, validation(employeeUpdateSchema), updateProfile)
  .delete("/:id", authToken, deleteUserEmployee);

module.exports = router;