const express = require('express');
const router = express.Router();
const { login, logout, register } = require('../controllers/auth.controller');

const { loginSchema, registerSchema } = require('../validations/auth.validation');
const validation = require('../middlewares/validation.middleware');
const authToken = require('../middlewares/auth.middleware');

router
  .post('/login', validation(loginSchema), login)
  .post('/logout', logout)
  .post('/register', authToken, validation(registerSchema), register);

module.exports = router;