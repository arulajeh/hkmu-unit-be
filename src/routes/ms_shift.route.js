const express = require("express");
const router = express.Router();
const {
  checkShift,
  create,
  update,
  endShift,
  getAll,
  startShift,
} = require("../controllers/ms_shift.controller");

const {
  shiftCreateSchema,
  shiftStartSchema,
  shiftEndSchema,
  shiftUpdateSchema
} = require("../validations/shift.validation");
const validation = require("../middlewares/validation.middleware");
const authToken = require("../middlewares/auth.middleware");

router
  .get("/", getAll)
  .get("/check", checkShift)
  .post("/", validation(shiftCreateSchema), create)
  .put("/", authToken, validation(shiftUpdateSchema), update)
  .post("/start", validation(shiftStartSchema), startShift)
  .post("/end", validation(shiftEndSchema), endShift);

module.exports = router;