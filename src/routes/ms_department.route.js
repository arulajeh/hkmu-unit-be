const express = require("express");
const router = express.Router();
const {
  getAll,
  create,
  getById,
  remove,
  update,
} = require("../controllers/ms_department.controller");

const { departmentCreateSchema, departmentUpdateSchema } = require("../validations/department.validation");
const validation = require("../middlewares/validation.middleware");
const authToken = require("../middlewares/auth.middleware");

router
  .get("/", getAll)
  .post("/", authToken, validation(departmentCreateSchema), create)
  .get("/:id", getById)
  .delete("/:id", authToken, remove)
  .put("/:id", authToken, validation(departmentUpdateSchema), update);

module.exports = router;
