const express = require("express");
const router = express.Router();
const authRouter = require("./auth.router");
const departmentRouter = require("./ms_department.route");
const lineRouter = require("./ms_line.route");
const employeeRouter = require("./ms_employee.route");
const shiftRouter = require("./ms_shift.route");

router
  .use("/auth", authRouter)
  .use("/departments", departmentRouter)
  .use("/lines", lineRouter)
  .use("/employees", employeeRouter)
  .use("/shifts", shiftRouter)
  .use((err, req, res, next) => {
    if (err) {
      res.status(500).json({
        message: "internal_server_error",
      });
    }
  });

module.exports = router;
