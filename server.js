const express = require('express');
const cors = require('cors');
const compression = require('compression');
const dotenv = require('dotenv');

dotenv.config();

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cors());
app.use(compression());

// open public folder to access static files
app.use(express.static('public'));

const router = require('./src/routes/index.router');
app.use('/api', router);

app.all('*', (req, res) => {
  res.status(404).send({ message: "Not Found" });
})

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});